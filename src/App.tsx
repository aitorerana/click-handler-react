import './App.css';
import { ClickHandler } from './components/ClickHandler';

function App() {
    const boxStyles = {
        border: '1px solid white',
        padding: '100px'
    };
    return (
        <div className="App">
            <header className="App-header">
                <ClickHandler onClickedInside={() => alert('You\'ve clicked INSIDE the box!')} onClickedOutside={() => alert('You\'ve clicked OUTSIDE the box!')}>
                    <div style={boxStyles}>BOX</div>
                </ClickHandler>
            </header>
        </div>
    );
}

export default App;
