import React from 'react';

export interface ClickHandlerProps {
    onClickedInside?: () => void,
    onClickedOutside?: () => void,
}

type ClickHandlerPropsSum = ClickHandlerProps;

class ClickHandler extends React.Component<ClickHandlerPropsSum> {

    private clickHandlerRef: any;

    constructor(props: ClickHandlerPropsSum) {
        super(props);
        this.clickHandlerRef = React.createRef();
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClick);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClick);
    }

    handleClick = (e: { target: any; }) => {
        const { onClickedOutside } = this.props;
        if (this.clickHandlerRef.current.contains(e.target)) {
            return;
        }
        if (onClickedOutside) {
            onClickedOutside();
        }
    }

    render () {
        const { onClickedInside } = this.props;
        return (
            <div ref={ this.clickHandlerRef } onClick={() => onClickedInside ? onClickedInside() : null}>
                {this.props.children}
            </div>
        );
    }
}

export { ClickHandler };
