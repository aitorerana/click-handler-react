# click-handler #

This is a React component to handle the click outside the children element.

### Requirements ###

This project requires Node 14 or higher.

### Configuration ###

1. Copy the *components/ClickHandler.tsx* component in your project.
2. Use the component:
```
<ClickHandler
    onClickedInside={() => function()}
    onClickedOutside={() => function()}
>
    {...children...}
</ClickHandler>
```

### Run the project ###

1. Run the command to install dependencies: ```npm install```
2. Run the command to start: ```npm start``` (Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.)

### Author ###

[Aitor Eraña](http://cv.aitorerana.com/ "CV aitorerana")